// prompt()
var para = document.getElementById('player-button');
para.addEventListener('click', updateName);

function updateName() {
    var name = prompt("Enter a new name");
    para.textContent = "Player 1: " + name;
}


// createElement(); addEventListener()
var buttons = window.document.getElementsByClassName("creator");
for (var i = 0; i < buttons.length; i++) {
    buttons[i].addEventListener('click', createParagraph);
    // equal to: buttons[i].onclick = createParagraph;
    // but can be removed or add multiple functions
}

function createParagraph() {
    var para = document.createElement('p');
    para.textContent = 'You clicked the button!';
    document.body.appendChild(para);
}


// events
function bgChange(e) {
    var rncCol = 'rgb(' + random(255) + ',' + random(255) + ',' + random(255) + ')';
    e.target.style.backgroundColor = rncCol;
    console.log(e);
}
var changeColorBtns = window.document.getElementsByClassName('change-color');
for (var i = 0; i < changeColorBtns.length; i++) {
    changeColorBtns[i].addEventListener('click', bgChange);
}
var changeColorDiv = window.document.getElementById('div-change-color');
changeColorDiv.addEventListener('click', bgChange);

// form validation; preventDefault()
var form = document.querySelector('form');
var fname = document.getElementById('fname');
var lname = document.getElementById('lname');
var submit = document.getElementById('submit');
var para = document.getElementById('warning-text');

form.onsubmit = function (e) {
    if (fname.value === '' || lname.value === '') {
        e.preventDefault();
        para.textContent = 'You need to fill in both names!';
    }
}


/* helper functions */
function random(num) {
    return Math.floor(Math.random() * num);
}
